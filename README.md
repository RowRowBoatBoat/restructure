# Discord Illuminati

This proposal is for the reform and restructure of the Discord Guild “Discord Illuminati”. It has been many months since Illuminati was made, and Discord has seen a lot of social changes. When Illuminati was made, it did not consider the future, nor did it plan well for its present. This outline will cover a new structure and set of rules and guidelines to replace the current.

---

## Table of Contents

* [Introduction](#introduction)
    * [Who and Why](#who-and-why)
    * [Terminology](#terminology)
* [Roles and the Hierarchy](#roles-and-the-hierarchy)
    * [Daggers](#daggers)
    * [Cloaks](#cloaks)
    * [Secretaries](#secretaries)
* [Procedures](#procedures)
    * [Membership](#membership)
    * [Termination](#termination)
    * [Elections](#elections)
* [Group Action](#group-action)
    * [Projects](#projects)
* [Voting](#voting)
    * [Quorum](#quorum)
* [Addendumns](#addendumns)

---

## Introduction

### Who and Why

We are a group of Discord owners and admins from some of the larger and high quality guilds around. The Illuminati exist as a center for networking between guilds, sharing information pertinent to guild management, planning for the future and improving member servers.

### Terminology

Throughout this document, the following phrases will be used to represent the following:

* Guild: Discord server
* Illuminati: us
* Open: Open, internally
* Public: Open to all
* Sponsor: Member leading a discussion

---

## Roles and the Hierarchy

Listed below are the new roles that are proposed for the guild. They are listed in hierarchical order.

### Daggers

Daggers are the admins of the guild. Their purpose is to host, and keep the guild moving smoothly. They are voted in by cloaks, and have no term limit.

### Cloaks

Cloaks are the legislators of the guild. There will be one for every 10 members of the guild. They are elected by the members (note, not the Daggers). Their purpose is to work towards the goals of the server.

### Secretaries

Secretaries are the record keepers of the guild. They keep track of all the projects, and the votes. They have no authority over members, just a title. They are voted in by cloaks, and have no term limit. Can be anyone but a Dagger.

--- 

## Procedures

Listed below are all the general procedures for the guild.

### Membership

Membership to the guild goes through the following process:

##### Pre-application

* A motion is required to prepare a Minor vote
* A [2/3 majority Minor Guild vote](#voting) is required to start interview and evaluation process
* Mandatory interview and evaluation
* Manual and automated observation
* After a week of observation, cloaks may call an admission vote
* If no vote is made after 4 weeks, the application fails.

##### Admission

Admission is determined by a [2/3 majority Minor Cloak vote](#voting), with the following Qualifiers and Disqualifiers being used to determine their vote.

##### Qualifiers

Must meet the following condition:

* Purpose aligns with Illuminati
    * Shitposting/Memes => Bad; PopularGameZ => Good; etc
* Must meet 2 of the remaining 4 conditions:
    * Ratio: active/total member count
        * Activity-to-Member Count: (Avg(Msgs/Min/Online User) Per day):Users = XX%
        * Exact % required will depend on the server. Some servers are meant to have more flowing conversations than others.
    * Ratio: age/member count
        * Age-Member Count Users:Days == XX%
    * 1000+ members
        * 1000 Members is after a 30 day prune (don’t actually have to do the prune)
* Structure
    * Organized, good use of Discord’s features, clear rules, systems of enforcement, etc

##### Disqualifiers

If any of the following conditions are met, they must be seriously taken into consideration, with a strong likelyhood of causing the vote to fail.

* Purpose (low impact)
    * Purpose as a disqualifier means it hits the opposite side of the spectrum, ie organized for something unethical or against the discord TOS.
* Known history (medium impact)
    * When an applicant is under consideration, a statement may be submitted for consideration. A vote will decide whether the statement is accepted as a valid consideration
 

##### Membership Benefits

Being a member of the Illuminati grants several benefits: 

* The Ticket
    * 3-7 staff, tbd during vote, more on request
* Rights
    * Not be dismissed without due process
    * Vote when guild or community structure is up for adjustment
    * Vote for Cloak positions

### Termination

All terminations must be voted on by a [2/3 majority Major Cloak vote](#voting), using the following criteria to determine their vote.

* Drama
* Hate speech/discrimination
* Persistent disregard for common rules (i.e. typical guild rules)
* Petition with a quorum of 8 members
* Disregard of other guilds rules (i.e. spamming, advertising, etc.)

A formal statement with evidence against the individual must be submitted. If a termination vote passes, the following procedure takes place:

* A formal statement must be written explaining why
* Depending on severity, statement is to be delivered before removal. If severe enough, statement is to be delivered after removal.
* Removal of user and/or guild from illuminati
* Statement must be delivered to who guild

### Elections

#### Administrators

##### Daggers

* No more, and no less than 7
* Voted in by a [1/2 majority Major Cloak vote](#voting), ties settled by a [2/3 majority Major Member vote](#voting).
* Nominated by a motion of 3 cloaks.

##### Cloaks

* One for every 10 members (cloaks and daggers included) of the guild
* Elections held Tri-Annually
    * 1st of January
    * 1st of May
    * 1st of September
* No more than 3 terms in 3 years
* Voted in by a [2/3 majority Major Member vote](#voting).
* Nominated by a motion of at least 15 members

##### Secretaries

* No more, and no less than 3.
* Voted in by a [1/2 majority Major Cloak vote](#voting), ties settled by a ]2/3 majority Major Dagger vote](#voting).
* Nominated by a motion of at least 10 members/cloaks

---

## Group Action

### Projects

Projects are the purpose of this guild.
* Must be started by a quorem of no less than 15% of the members, OR by a [1/2 majority Minor Cloak vote](#voting).
* Must have a formal outline and objective.
* Must be guided by at least one Cloak or Dagger.
* A [1/2 majority Minor Guild vote](#voting) can disband and/or detach the project from the Illuminati.

--- 

## Voting

Voting within the guild must follow the following guidelines

### Motions

A motion must be brought up, to start a discussion on the topic. If a motion does not receive a second, it will not get a discussion.

### Discussions

Discussions will take place in public channels (likely created automatically by a bot after a motion is seconded), and announced in a specific channel. 
Discussions can last as long as the sponsors want. The first act of the group should be to decide who the Sponsors are.  A discussion must have two sponsors,
and a secretary. 

### Call for Vote

When a sponsors feel the vote stands enough chance for a vote, they may call for a vote (likely done using the bot).

### The Vote

Votes will last for a period of two? weeks, or until the last member casts their vote. Votes cast after will not be taken into account.
Votes can be "In Favor", "Against", or "Abstain".

### Quorum:

A quorum is required for any and all votes in the Illuminati. The necessary quorum is dependent on the type of vote. The different quorums are listed below.

* Major Dagger: At least 6 of the 7 daggers
* Minor Dagger: At least 4 of the 7 daggers
* Major Cloak: At least 90% of the cloaks
* Minor Cloak: At least 75% of the cloaks
* Major Guild: At least 80% of the members and 75% of the cloaks and 25% of the daggers
* Minor Guild: At least 40% of the members and 50% of the cloaks and 1 dagger
* Major Member: At least 70% of the members (non-dagger)
* Minor Member: At least 30% of the members (non-dagger) with at least 1 cloak/dagger
* Motion: Requires at least two (or more, if specified). 

### Results

Results will be publically posted after the vote has ended. If the vote passes, it has one week to be implemented, unless specified in the voting documents. It will also automatically close the discussion.

If the vote fails, it may return to the discussion room, or, if the sponsors wish to end the discussion, it can be closed.

### Issues

If a non-Dagger claims a Cloak has bias, Daggers rule on whether the claim is valid with a 2/3 majority Major Dagger vote. If valid, said Cloak does not get a vote, and is not included in the quroum.
Votes will be done via a Discord Bot that is ran via a Deployment system which pulls directly from the source repository, with an open-view shell

---

## Addendumns

This section is akin to Amendments of the Constitution. As with actual legal documents, nothing is ever trashed after enacted, it's merely revoked/annulled

Addendumns can be brought forward, with a motion from at least 3 members/cloaks/daggers. They must be well thought out, 

